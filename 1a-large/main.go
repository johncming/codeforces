package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	s := bufio.NewScanner(os.Stdin)

	for s.Scan() {
		res := processLine(s.Text())
		fmt.Println(res)
	}

	if err := s.Err(); err != nil {
		panic(err)
	}
}

func panicErr(err error) {
	if err != nil {
		panic(err)
	}
}

func processLine(line string) string {
	tokens := strings.Split(line, " ")
	ns, ms, as := tokens[0], tokens[1], tokens[2]

	n, err := strconv.ParseUint(ns, 10, 64)
	panicErr(err)
	m, err := strconv.ParseUint(ms, 10, 64)
	panicErr(err)
	a, err := strconv.ParseUint(as, 10, 64)
	panicErr(err)

	res := process(n, m, a)
	return fmt.Sprintf("%d", uint(res))
}

func process(n, m, a uint64) uint64 {
	in := n / a
	if in*a < n {
		in++
	}

	im := m / a
	if im*a < m {
		im++
	}

	return in * im

}
