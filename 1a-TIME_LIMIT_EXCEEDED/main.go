package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	s := bufio.NewScanner(os.Stdin)

	for s.Scan() {
		res := processLine(s.Text())
		fmt.Println(res)
	}

	if err := s.Err(); err != nil {
		panic(err)
	}
}

func processLine(line string) string {
	tokens := strings.Split(line, " ")
	ns, ms, as := tokens[0], tokens[1], tokens[2]

	n, _ := strconv.Atoi(ns)
	m, _ := strconv.Atoi(ms)
	a, _ := strconv.Atoi(as)

	res := process(n, m, a)
	return fmt.Sprintf("%d", res)
}

func process(n, m, a int) int {
	in := 0
	im := 0

	for {
		if (in+1)*a == n {
			in++
			break
		} else if (in+1)*a < n {
			in++
		} else {
			in++
			break
		}

	}

	for {
		if (im+1)*a == m {
			im++
			break
		} else if (im+1)*a < m {
			im++
		} else {
			im++
			break
		}
	}

	return in * im
}
